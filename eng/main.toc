\babel@toc {english}{}
\beamer@sectionintoc {1}{Additive manufacturing}{3}{0}{1}
\beamer@sectionintoc {2}{DMLS}{4}{0}{2}
\beamer@sectionintoc {3}{When simulation makes sense?}{5}{0}{3}
\beamer@sectionintoc {4}{Challenges of simulations}{6}{0}{4}
\beamer@sectionintoc {5}{Future vision}{7}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{8}{0}{6}
\beamer@sectionintoc {7}{References}{9}{0}{7}
